package br.com.rdportal.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.rdportal.dto.IndexDTO;
import br.com.rdportal.enums.IndexType;
import br.com.rdportal.model.IndexComponent;
import br.com.rdportal.repository.IndexComponentRepository;

@Service
public class IndexService {
	
	@Autowired
	IndexComponentRepository indexRepository;
	
	@Transactional
	public List<IndexComponent> save(List<IndexDTO> indexComponents) {
		List<IndexComponent> componentsAdded = new ArrayList<IndexComponent>();
		try {
			for (IndexDTO index : indexComponents) {
				IndexComponent indexNew = new IndexComponent();
				indexNew.setType(IndexType.valueOf(index.getType()));
				indexNew.setCategory(index.getCategory());
				indexNew.setZindex(index.getZindex());
				indexNew.setVisibility(index.getVisibility());
				
				componentsAdded.add(indexRepository.save(indexNew));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	    return componentsAdded;
	}
	
	@Transactional
	public List<IndexComponent> update(List<IndexDTO> indexComponents) {
		List<IndexComponent> componentsUpdated = new ArrayList<IndexComponent>();
		try {
			indexRepository.deleteAll();
			
			componentsUpdated = this.save(indexComponents);
		} catch (Exception e) {
			e.printStackTrace();
		}

	    return componentsUpdated;
	}
	
	public List<IndexComponent> getAllComponent() {
		return indexRepository.findAll();
	}
}
