package br.com.rdportal.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.rdportal.dto.UserDTO;
import br.com.rdportal.model.StorageFile;
import br.com.rdportal.security.Authority;
import br.com.rdportal.security.AuthorityRepository;
import br.com.rdportal.security.Usuario;
import br.com.rdportal.security.UsuarioRepository;

@Service
public class UserService {
	
	@Autowired
	private UsuarioRepository userRepository;
	
	@Autowired
	private AuthorityRepository authorityRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private S3Service s3Service;
	
	@Autowired
	private StorageService storageService;
	
	@Transactional
	public Usuario save(UserDTO usuario) {
		Usuario user = new Usuario();

		try {
			user.setUsername(usuario.getUsername());
			user.setPassword(passwordEncoder.encode(usuario.getPassword()));
			user.setAuthorities(usuario.getAuthorities());
			user.setNomeCompleto(usuario.getNomeCompleto());
			user.setEmail(usuario.getEmail());
			user.setImage(usuario.getImage());
			user.setCategories(usuario.getCategories());
			user.setPublicName(usuario.getPublicName());
			user.setPublicDescription(usuario.getPublicDescription());
			user = userRepository.save(user);
		} catch (Exception e) {
			e.printStackTrace();
		}

	    return user;
	}
	
	public Usuario getByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	public List<Usuario> getAllUsers() {
		return userRepository.findAll();
	}
	
	public List<Authority> getAuthorities() {
		return authorityRepository.findAll();
	}
	
	public Boolean deleteUser(Integer id) {
		try {			
			Usuario user = userRepository.getOne(id);
			if (user.getImage() != null) {				
				String url = user.getImage().getUrl();
				s3Service.deleteFileFromS3Bucket(url);
			}
			userRepository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Transactional
	public Usuario updateUser(UserDTO usuario) {
		Usuario user = new Usuario();
		try {
			user = userRepository.getOne(usuario.getId());
			user.setAuthorities(usuario.getAuthorities());
			user.setNomeCompleto(usuario.getNomeCompleto());
			user.setUsername(usuario.getUsername());
			user.setEmail(usuario.getEmail());
			user.setCategories(usuario.getCategories());
			user.setPublicName(usuario.getPublicName());
			user.setPublicDescription(usuario.getPublicDescription());
			
			if (usuario.getPassword() != null) {
				user.setPassword(passwordEncoder.encode(usuario.getPassword()));
			}
			
			StorageFile file = user.getImage();
			user.setImage(usuario.getImage());
			
			userRepository.saveAndFlush(user);
			
			if (!file.getUrl().equals(user.getImage().getUrl())) {				
				storageService.deleteStorageFile(file.getId());
				s3Service.deleteFileFromS3Bucket(file.getUrl());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
	public Usuario getUser(Integer id) {
		return userRepository.getOne(id);
	}

}
