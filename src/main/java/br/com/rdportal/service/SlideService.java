package br.com.rdportal.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.rdportal.model.Slide;
import br.com.rdportal.repository.SlideRepository;

@Service
public class SlideService {
	
	@Autowired
	SlideRepository slideRepository;
	
	@Transactional
	public List<Slide> save(List<Slide> slides) {
		List<Slide> slideAdded = new ArrayList<Slide>();
		try {
			for (Slide slide : slides) {
				Slide slideNew = new Slide();
				slideNew.setPost(slide.getPost());
				slideNew.setZindex(slide.getZindex());
				slideNew.setVisibility(slide.getVisibility());
				
				slideAdded.add(slideRepository.save(slideNew));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	    return slideAdded;
	}
	
	@Transactional
	public List<Slide> update(List<Slide> slides) {
		List<Slide> slidesUpdated = new ArrayList<Slide>();
		try {
			slideRepository.deleteAll();
			
			slidesUpdated = this.save(slides);
		} catch (Exception e) {
			e.printStackTrace();
		}

	    return slidesUpdated;
	}
	
	public List<Slide> getAllSlides() {
		return slideRepository.findAll();
	}

}
