package br.com.rdportal.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.rdportal.model.Menu;
import br.com.rdportal.repository.MenuRepository;

@Service
public class MenuService {
	
	@Autowired
	MenuRepository menuRepository;
	
	@Transactional
	public List<Menu> save(List<Menu> menu) {
		List<Menu> menuAdded = new ArrayList<Menu>();
		try {
			for (Menu option : menu) {
				Menu menuNew = new Menu();
				menuNew.setCategory(option.getCategory());;
				menuNew.setZindex(option.getZindex());
				menuNew.setVisibility(option.getVisibility());
				
				menuAdded.add(menuRepository.save(menuNew));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	    return menuAdded;
	}
	
	@Transactional
	public List<Menu> update(List<Menu> menu) {
		List<Menu> menuUpdated = new ArrayList<Menu>();
		try {
			menuRepository.deleteAll();
			
			menuUpdated = this.save(menu);
		} catch (Exception e) {
			e.printStackTrace();
		}

	    return menuUpdated;
	}
	
	public List<Menu> getAllMenu() {
		return menuRepository.findAll();
	}

}
