package br.com.rdportal.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.rdportal.dto.PostDTO;
import br.com.rdportal.enums.PostType;
import br.com.rdportal.model.Post;
import br.com.rdportal.model.PostForm;
import br.com.rdportal.model.PostText;
import br.com.rdportal.model.StorageFile;
import br.com.rdportal.repository.PostFormRepository;
import br.com.rdportal.repository.PostRepository;
import br.com.rdportal.repository.PostTextRepository;
import br.com.rdportal.security.UsuarioRepository;

@Service
public class PostService {
	
	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private PostTextRepository postTextRepository;
	
	@Autowired
	private PostFormRepository postFormRepository;
	
	@Autowired
	private S3Service s3Service;
	
	@Autowired
	private StorageService storageService;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Transactional
	public Post save(PostDTO post, String username) {
		Post postNew = new Post();
		try {
			postNew.setCreatedBy(usuarioRepository.findByUsername(username));
			
			if (post.getType() == PostType.postText) {				
				postNew.setPostText(savePostText(post.getPostText()));
				postNew.setCategories(post.getCategories());
			}
			if (post.getType() == PostType.postForm) {
				postNew.setPostForm(savePostForm(post.getPostForm()));
				postNew.setCategories(post.getCategories());
			}
			postNew = postRepository.save(postNew);
		} catch (Exception e) {
			e.printStackTrace();
		}

	    return postNew;
	}
	
	@Transactional
	public Post updatePost(PostDTO post) {
		Post postUp = new Post();
		try {
			postUp = postRepository.getOne(post.getId());
			
			StorageFile file = post.getPostText().getImage();
			
			if (post.getType() == PostType.postText) {				
				postUp.setPostText(post.getPostText());
			}
			
			if (post.getType() == PostType.postForm) {
				postUp.setPostForm(post.getPostForm());
			}
			postUp.setCategories(post.getCategories());
			
			postRepository.saveAndFlush(postUp);
			
			if (!postUp.getPostText().getImage().getUrl().equals(file.getUrl())) {				
				storageService.deleteStorageFile(file.getId());
				s3Service.deleteFileFromS3Bucket(file.getUrl());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return postUp;
	}
	
	@Transactional
	public PostText savePostText(PostText postToSave) {
		PostText postText = new PostText();
		postText.setTitle(postToSave.getTitle());
		postText.setSubtitle(postToSave.getSubtitle());
		postText.setContent(postToSave.getContent());
		postText.setImage(postToSave.getImage());
		
		return postTextRepository.save(postText);
	}
	
	@Transactional
	public PostForm savePostForm(PostForm postToSave) {
		PostForm postForm = new PostForm();
		postForm.setTitle(postToSave.getTitle());
		postForm.setSubtitle(postToSave.getSubtitle());
		postForm.setQuestions(postToSave.getQuestions());
		postForm.setImage(postToSave.getImage());
		
		return postFormRepository.save(postForm);
	}
	
	@Transactional
	public List<Post> getAllPosts() {
		return postRepository.findAll();
	}
	
	@Transactional
	public List<Post> getPostsWithPagination(Integer pageNo, Integer pageSize, String sortBy) {
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).descending());
		Page<Post> pagedResult = postRepository.findAll(paging);
		
		if (pagedResult.hasContent()) {
			return pagedResult.getContent();
		} else {
			return new ArrayList<Post>();
		}
	}
	
	@Transactional
	public List<Post> getPostsByCategoryWithPagination(Integer pageNo, Integer pageSize, String sortBy, Integer id) {
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).descending());
		Page<Post> pagedResult = postRepository.findAllByCategoryWithPagination(paging, id);
		
		if(pagedResult.hasContent()) {
			return pagedResult.getContent();
		} else {
			return new ArrayList<Post>();
		}
	}
	
	@Transactional
	public List<Post> getPostsBySearchWithPagination(Integer pageNo, Integer pageSize, String sortBy, String search) {
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).descending());
		Page<Post> postResult = postRepository.findAllPostBySearchWithPagination(paging, search);
		
		if (postResult.hasContent()) {
			return postResult.getContent();
		}
		
		return new ArrayList<Post>();
	}
	
	@Transactional
	public Boolean deletePost(Integer id) {
		try {			
			postRepository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Transactional
	public Post getPost(Integer id) {
		return postRepository.getOne(id);
	}

}
