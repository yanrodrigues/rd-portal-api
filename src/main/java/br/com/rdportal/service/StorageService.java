package br.com.rdportal.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.rdportal.model.StorageFile;
import br.com.rdportal.repository.StorageFileRepository;
import br.com.rdportal.security.UsuarioRepository;

@Service
@Transactional
public class StorageService {

    @Autowired
    StorageFileRepository storageFileRepository;
    
    @Autowired
    UsuarioRepository userRepository;

    public StorageFile save(String url) {
        StorageFile storageFile = new StorageFile();
        storageFile.setUrl(url);
        storageFileRepository.save(storageFile);
        return  storageFile;
    }

    public StorageFile findStorageFile(Integer id) {
        return storageFileRepository.getOne(id);
    }
    
    @Transactional
    public void deleteStorageFile(Integer id) {
    	try {    		
    		storageFileRepository.deleteById(id);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }

}
