package br.com.rdportal.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.rdportal.dto.CategoryDTO;
import br.com.rdportal.model.Category;
import br.com.rdportal.model.StorageFile;
import br.com.rdportal.repository.CategoryRepository;

@Service
public class CategoryService {
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private S3Service s3Service;
	
	@Autowired
	private StorageService storageService;
	
	@Transactional
	public Category saveCategory(CategoryDTO category) {
		Category categoryNew = new Category();
		
		try {
			categoryNew.setName(category.getName());
			categoryNew.setDescription(category.getDescription());
			categoryNew.setImage(category.getImage());
			
			categoryNew = categoryRepository.save(categoryNew);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return categoryNew;
	}
	
	public List<Category> getAllCategory() {
		return categoryRepository.findAll();
	}
	
	public Boolean deleteCategory(Integer id) {
		try {			
			Category category = categoryRepository.getOne(id);
			if (category.getImage() != null) {				
				String url = category.getImage().getUrl();
				s3Service.deleteFileFromS3Bucket(url);
			}
			categoryRepository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Transactional
	public Category updateCategory(CategoryDTO category) {
		Category categoryUp = new Category();
		try {
			categoryUp = categoryRepository.getOne(category.getId());
			categoryUp.setName(category.getName());
			categoryUp.setDescription(category.getDescription());
			
			StorageFile file = category.getImage();
			categoryUp.setImage(category.getImage());
			
			categoryRepository.saveAndFlush(categoryUp);
			
			if (!categoryUp.getImage().getUrl().equals(file.getUrl())) {				
				storageService.deleteStorageFile(file.getId());
				s3Service.deleteFileFromS3Bucket(file.getUrl());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return categoryUp;
	}
	
	public Category getCategory(Integer id) {
		return categoryRepository.getOne(id);
	}

}
