package br.com.rdportal.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.rdportal.model.Post;

public interface PostRepository extends JpaRepository<Post, Integer> {
	
	@Query("select p from Post p join p.categories c where c.id = :id")
	Page<Post> findAllByCategoryWithPagination(Pageable pageable, @Param("id")Integer id);
	
	@Query("SELECT DISTINCT p FROM Post p JOIN p.categories c "
			+ "WHERE p.id IN (SELECT p.id FROM Post p JOIN p.postText pt WHERE lower(pt.title) LIKE lower(concat('%', :search, '%')) OR lower(pt.subtitle) LIKE lower(concat('%', :search, '%'))) "
			+ "OR p.id IN (SELECT p.id FROM Post p JOIN p.postForm pf WHERE lower(pf.title) LIKE lower(concat('%', :search, '%')) OR lower(pf.subtitle) LIKE lower(concat('%', :search, '%'))) "
			+ "OR lower(c.name) LIKE lower(concat('%', :search, '%'))")
	Page<Post> findAllPostBySearchWithPagination(Pageable pageable, @Param("search")String search);
	
}
