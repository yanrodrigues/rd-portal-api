package br.com.rdportal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.rdportal.model.PostText;

public interface PostTextRepository extends JpaRepository<PostText, Integer> {

}
