package br.com.rdportal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.rdportal.model.Slide;

public interface SlideRepository extends JpaRepository<Slide, Integer> {

}
