package br.com.rdportal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.rdportal.model.Menu;

public interface MenuRepository extends JpaRepository<Menu, Integer> {

}
