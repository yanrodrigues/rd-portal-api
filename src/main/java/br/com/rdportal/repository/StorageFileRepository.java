package br.com.rdportal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.rdportal.model.StorageFile;

public interface StorageFileRepository extends JpaRepository<StorageFile, Integer> {
    
}