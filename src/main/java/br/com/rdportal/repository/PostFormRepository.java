package br.com.rdportal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.rdportal.model.PostForm;

public interface PostFormRepository extends JpaRepository<PostForm, Integer> {

}
