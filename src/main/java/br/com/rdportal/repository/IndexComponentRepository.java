package br.com.rdportal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.rdportal.model.IndexComponent;


public interface IndexComponentRepository extends JpaRepository<IndexComponent, Integer> {

}
