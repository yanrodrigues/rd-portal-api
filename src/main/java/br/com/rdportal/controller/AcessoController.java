package br.com.rdportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.access.prepost.PreAuthorize;

import br.com.rdportal.dto.UserDTO;
import br.com.rdportal.security.Authority;
import br.com.rdportal.security.Usuario;
import br.com.rdportal.service.UserService;

@RestController
public class AcessoController {
	
	@Autowired
	UserService userService;
	
	@PostMapping("/register")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Usuario> createNewUser(@RequestBody UserDTO usuario){
		return ResponseEntity.ok().body(userService.save(usuario));
	}
	
	@GetMapping("/user")
	@PreAuthorize("hasAnyRole('USER', 'MODERATOR', 'ADMIN')")
	public ResponseEntity<Usuario> getUserByUsername(@RequestParam("username") String username){
		return ResponseEntity.ok().body(userService.getByUsername(username));
	}
	
	@GetMapping("/authorities")
	@PreAuthorize("hasAnyRole('USER', 'MODERATOR', 'ADMIN')")
	public ResponseEntity<List<Authority>> getAuthorities() {
		return ResponseEntity.ok().body(userService.getAuthorities());
	}

}
