package br.com.rdportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.rdportal.dto.IndexDTO;
import br.com.rdportal.model.IndexComponent;
import br.com.rdportal.service.IndexService;

@RestController
@RequestMapping("/index")
public class IndexController {
	
	@Autowired
	private IndexService indexService;
	
	@PostMapping("/register")
	@PreAuthorize("hasAnyRole('MODERATOR', 'ADMIN')")
	public ResponseEntity<List<IndexComponent>> createIndexComponentList(@RequestBody List<IndexDTO> components){
		return ResponseEntity.ok().body(indexService.save(components));
	}
	
	@PutMapping
	@PreAuthorize("hasAnyRole('MODERATOR', 'ADMIN')")
	public ResponseEntity<List<IndexComponent>> updateIndexComponentList(@RequestBody List<IndexDTO> components){
		return ResponseEntity.ok().body(indexService.update(components));
	}
	
	@GetMapping
	public ResponseEntity<List<IndexComponent>> getAllComponent() {
		return ResponseEntity.ok().body(indexService.getAllComponent());
	}

}
