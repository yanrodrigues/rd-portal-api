package br.com.rdportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.rdportal.model.Slide;
import br.com.rdportal.service.SlideService;

@RestController
@RequestMapping("/slide")
public class SlideController {
	
	@Autowired
	SlideService slideService;
	
	@PostMapping("/register")
	@PreAuthorize("hasAnyRole('MODERATOR', 'ADMIN')")
	public ResponseEntity<List<Slide>> createSlideList(@RequestBody List<Slide> slides){
		return ResponseEntity.ok().body(slideService.save(slides));
	}
	
	@PutMapping
	@PreAuthorize("hasAnyRole('MODERATOR', 'ADMIN')")
	public ResponseEntity<List<Slide>> updateSlideList(@RequestBody List<Slide> slides){
		return ResponseEntity.ok().body(slideService.update(slides));
	}
	
	@GetMapping
	public ResponseEntity<List<Slide>> getAllSlide() {
		return ResponseEntity.ok().body(slideService.getAllSlides());
	}

}
