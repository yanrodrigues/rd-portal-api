package br.com.rdportal.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.rdportal.dto.PostDTO;
import br.com.rdportal.model.Post;
import br.com.rdportal.service.PostService;

@RestController
@RequestMapping("/post")
public class PostController {
	
	@Autowired
	private PostService postService;
	
	@PostMapping("/register")
	@PreAuthorize("hasAnyRole('USER', 'MODERATOR', 'ADMIN')")
	public ResponseEntity<Post> createNewPost(@RequestBody PostDTO post, Principal principal){
		return ResponseEntity.ok().body(postService.save(post, principal.getName()));
	}
	
	@GetMapping
	public ResponseEntity<List<Post>> getAllPosts() {
		return ResponseEntity.ok().body(postService.getAllPosts());
	}
	
	@GetMapping("/pages")
	public ResponseEntity<List<Post>> getPostsWithPagination(
			@RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy) {
		return ResponseEntity.ok().body(postService.getPostsWithPagination(pageNo, pageSize, sortBy));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Post> getPostById(@PathVariable Integer id) {
		return ResponseEntity.ok().body(postService.getPost(id));
	}
	
	@GetMapping("/category/{id}/pages")
	public ResponseEntity<List<Post>> getPostsByCategoryWithPagination(
			@RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy,
            @PathVariable Integer id) {
		return ResponseEntity.ok().body(postService.getPostsByCategoryWithPagination(pageNo, pageSize, sortBy, id));
	}
	
	@GetMapping("/search/pages")
	public ResponseEntity<List<Post>> getPostsBySearchWithPagination(
			@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize,
			@RequestParam(defaultValue = "id") String sortBy,
			@RequestParam(defaultValue = "") String search) {
		return ResponseEntity.ok().body(postService.getPostsBySearchWithPagination(pageNo, pageSize, sortBy, search));
	}
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAnyRole('USER', 'MODERATOR', 'ADMIN')")
	public ResponseEntity<Boolean> deletePost(@PathVariable Integer id) {
		return ResponseEntity.ok().body(postService.deletePost(id));
	}
	
	@PutMapping
	@PreAuthorize("hasAnyRole('USER', 'MODERATOR', 'ADMIN')")
	public ResponseEntity<Post> updatePost(@RequestBody PostDTO post) {
		return ResponseEntity.ok().body(postService.updatePost(post));
	}

}
