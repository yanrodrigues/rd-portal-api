package br.com.rdportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.rdportal.dto.CategoryDTO;
import br.com.rdportal.model.Category;
import br.com.rdportal.service.CategoryService;

@RestController
@RequestMapping("/category")
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/all")
	public ResponseEntity<List<Category>> getAllCategories() {
		return ResponseEntity.ok().body(categoryService.getAllCategory());
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Category> getCategoryById(@PathVariable Integer id) {
		return ResponseEntity.ok().body(categoryService.getCategory(id));
	}
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAnyRole('MODERATOR', 'ADMIN')")
	public ResponseEntity<Boolean> deleteCategory(@PathVariable Integer id) {
		return ResponseEntity.ok().body(categoryService.deleteCategory(id));
	}
	
	@PutMapping
	@PreAuthorize("hasAnyRole('MODERATOR', 'ADMIN')")
	public ResponseEntity<Category> updateCategory(@RequestBody CategoryDTO category) {
		return ResponseEntity.ok().body(categoryService.updateCategory(category));
	}
	
	@PostMapping("/register")
	@PreAuthorize("hasAnyRole('MODERATOR', 'ADMIN')")
	public ResponseEntity<Category> createNewCategory(@RequestBody CategoryDTO category){
		return ResponseEntity.ok().body(categoryService.saveCategory(category));
	}

}
