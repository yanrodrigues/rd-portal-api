package br.com.rdportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.ResponseEntity;

import br.com.rdportal.model.StorageFile;
import br.com.rdportal.service.S3Service;

@RestController
@RequestMapping("/storage/")
public class StorageController {
	private S3Service s3Service;

    @Autowired
    void BucketController(S3Service s3Service) {
        this.s3Service = s3Service;
    }

    @PostMapping("/file")
    @PreAuthorize("hasAnyRole('USER', 'MODERATOR','ADMIN')")
    public ResponseEntity<StorageFile> uploadFile(@RequestPart(value = "file") MultipartFile file) {
        return ResponseEntity.ok().body(s3Service.uploadFile(file));
    }
}
