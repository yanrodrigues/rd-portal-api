package br.com.rdportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.rdportal.model.Menu;
import br.com.rdportal.service.MenuService;

@RestController
@RequestMapping("/menu")
public class MenuController {
	
	@Autowired
	MenuService menuService;
	
	@PostMapping("/register")
	@PreAuthorize("hasAnyRole('MODERATOR', 'ADMIN')")
	public ResponseEntity<List<Menu>> createMenuList(@RequestBody List<Menu> menu){
		return ResponseEntity.ok().body(menuService.save(menu));
	}
	
	@PutMapping
	@PreAuthorize("hasAnyRole('MODERATOR', 'ADMIN')")
	public ResponseEntity<List<Menu>> updateMenuList(@RequestBody List<Menu> menu){
		return ResponseEntity.ok().body(menuService.update(menu));
	}
	
	@GetMapping
	public ResponseEntity<List<Menu>> getAllMenu() {
		return ResponseEntity.ok().body(menuService.getAllMenu());
	}

}
