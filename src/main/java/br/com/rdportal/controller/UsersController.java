package br.com.rdportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping ;

import br.com.rdportal.dto.UserDTO;
import br.com.rdportal.security.Usuario;
import br.com.rdportal.service.UserService;

@RestController
@RequestMapping("/users")
public class UsersController {
	
	@Autowired
	UserService userService;
	
	@GetMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<List<Usuario>> getAllUsers() {
		return ResponseEntity.ok().body(userService.getAllUsers());
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAnyRole('MODERATOR', 'ADMIN', 'USER')")
	public ResponseEntity<Usuario> getUserById(@PathVariable Integer id) {
		return ResponseEntity.ok().body(userService.getUser(id));
	}
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Boolean> deleteUser(@PathVariable Integer id) {
		return ResponseEntity.ok().body(userService.deleteUser(id));
	}
	
	@PutMapping
	@PreAuthorize("hasAnyRole('USER', 'MODERATOR', 'ADMIN')")
	public ResponseEntity<Usuario> updateUser(@RequestBody UserDTO usuario) {
		return ResponseEntity.ok().body(userService.updateUser(usuario));
	}
}
