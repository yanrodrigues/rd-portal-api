package br.com.rdportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RdPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(RdPortalApplication.class, args);
	}

}
