package br.com.rdportal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.rdportal.enums.IndexType;

@Entity
@Table(name="index_component")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IndexComponent {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	
    @Enumerated(EnumType.STRING)
    private IndexType type;
	
	@ManyToOne()
	@JoinColumn(name = "category_id", referencedColumnName = "id")
	private Category category;
	
	private Integer zindex;
	
	@Column(nullable = false)
	private Boolean visibility;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public IndexType getType() {
		return type;
	}
	
	public void setType(IndexType type) {
		this.type = type;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Integer getZindex() {
		return zindex;
	}

	public void setZindex(Integer zindex) {
		this.zindex = zindex;
	}
	
	public Boolean getVisibility() {
		return visibility;
	}
	
	public void setVisibility(Boolean visibility) {
		this.visibility = visibility;
	}
    
	@PreRemove
	private void removeCategory() {
		category = null;
	}

}
