package br.com.rdportal.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.rdportal.security.Usuario;

@Entity
@Table(name="post")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Post {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "post_text_id", referencedColumnName = "id")
	private PostText postText;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "post_form_id", referencedColumnName = "id")
	private PostForm postForm;
	
	@ManyToMany()
    @JoinTable(name = "post_category",
            joinColumns = @JoinColumn(name = "post_id", referencedColumnName = "id") ,
            inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id"))
	private List<Category> categories;
	

    @OneToMany(mappedBy = "post", orphanRemoval = true)
    private List<Slide> slides;
    
    private Date dateCreated = new Date();
    
    private Date dateUpdated = new Date();
    
	@ManyToOne()
	@JoinColumn(name = "createdBy", referencedColumnName = "id")
    private Usuario createdBy;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PostText getPostText() {
		return postText;
	}

	public void setPostText(PostText postText) {
		this.postText = postText;
	}
	
	public PostForm getPostForm() {
		return postForm;
	}

	public void setPostForm(PostForm postForm) {
		this.postForm = postForm;
	}

	public List<Category> getCategories() {
		return categories;
	}
	
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
    
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public Usuario getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Usuario createdBy) {
		this.createdBy = createdBy;
	}

	@PreRemove
	private void preRemove() {
		categories.clear();
		createdBy = null;
	}
	
	@PreUpdate
	private void preUpdate() {
		this.dateUpdated = new Date();
	}

}
