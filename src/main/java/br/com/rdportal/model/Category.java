package br.com.rdportal.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.rdportal.security.Usuario;

@Entity
@Table(name="category")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Category {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	
	@NotEmpty
    @Column(nullable = false, unique = true)
	private String name = "";
	
	private String description = "";

	@OneToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "image_id", referencedColumnName = "id")
	private StorageFile image;
    
    @ManyToMany(mappedBy = "categories")
    List<Post> posts;
    
    @OneToMany(mappedBy = "category", orphanRemoval = true)
    List<IndexComponent> components;
    
    @ManyToMany(mappedBy = "categories")
    List<Usuario> usuarios;
	
	public Category(){}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public StorageFile getImage() {
		return image;
	}
	public void setImage(StorageFile image) {
		this.image = image;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
	@PreRemove
	private void removePosts() {
		posts.clear();
	}

}
