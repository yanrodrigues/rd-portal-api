package br.com.rdportal.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name="post_form")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PostForm {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

	@NotEmpty
    @Column(nullable = false)
	private String title;

	@NotEmpty
    @Column(nullable = false)
	private String subtitle;

	@NotEmpty
    @Column(nullable = false)
	private String[] questions;

	@OneToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "image_id", referencedColumnName = "id")
	private StorageFile image;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String[] getQuestions() {
		return questions;
	}

	public void setQuestions(String[] questions) {
		this.questions = questions;
	}

	public StorageFile getImage() {
		return image;
	}

	public void setImage(StorageFile image) {
		this.image = image;
	}

}
