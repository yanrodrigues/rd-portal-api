package br.com.rdportal.model;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name="post_text")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PostText {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

	@NotEmpty
    @Column(nullable = false)
	private String title;

	@NotEmpty
    @Column(nullable = false)
	private String subtitle;

	@NotEmpty
	@Lob
    @Column(nullable = false)
	@Basic(fetch=FetchType.EAGER)
	@Type(type = "org.hibernate.type.TextType")
	private String content;

	@OneToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "image_id", referencedColumnName = "id")
	private StorageFile image;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public StorageFile getImage() {
		return image;
	}

	public void setImage(StorageFile image) {
		this.image = image;
	}

}
