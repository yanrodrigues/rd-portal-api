package br.com.rdportal.security;

public enum AuthorityType {
	ROLE_ADMIN,
	ROLE_MODERATOR,
	ROLE_USER
}