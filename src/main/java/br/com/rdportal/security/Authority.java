package br.com.rdportal.security;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "authority")
public class Authority {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="seq_authority")
    @GenericGenerator(name = "seq_authority", strategy="increment")
    private Integer id;
    
    @Enumerated(EnumType.STRING)
    private AuthorityType name;
    
    @ManyToMany(mappedBy = "authorities")
    List<Usuario> usuarios;
    
    public Authority() {}
    
    public Authority(AuthorityType name) {
        this.name = name;
    }
    
    public String getName() {
    	return this.name.toString();
    }
    
    public Integer getId() {
    	return this.id;
    }

}
