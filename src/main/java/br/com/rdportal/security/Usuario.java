package br.com.rdportal.security;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.rdportal.model.Category;
import br.com.rdportal.model.Post;
import br.com.rdportal.model.StorageFile;

@Entity
@Table(name="usuario")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Usuario implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator="seq_usuario")
	@GenericGenerator(name = "seq_usuario", strategy="increment")
	private Integer id;
	
	@NotEmpty
    @Column(nullable = false, unique = true)
	private String username;
	
    @NotEmpty
    @JsonIgnore
	private String password;
	
	private Date dateCreated = new Date();
	
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_authority",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id") ,
            inverseJoinColumns = @JoinColumn(name = "authority_id", referencedColumnName = "id"))
	private List<Authority> authorities;
	
	private String nomeCompleto = "";
	private String email = "";
	
	private String publicName = "";
	private String publicDescription = "";

	@OneToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "image_id", referencedColumnName = "id")
	private StorageFile image;
	
	@OneToMany(mappedBy = "createdBy", orphanRemoval = false)
	private List<Post> posts;
	
	@ManyToMany()
	@JoinTable(name = "user_categories",
				joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
				inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id"))
	private List<Category> categories;

    public Usuario() {
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public List<Authority> getAuthorities() {
		return authorities;
	}
	public void setAuthorities(List<Authority> authorities) {
		this.authorities = authorities;
	}
	public void addAuthority(Authority authority) {
		this.authorities.add(authority);
	}

	public String getNomeCompleto() {
		return nomeCompleto;
	}
	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public StorageFile getImage() {
		return image;
	}
	public void setImage(StorageFile image) {
		this.image = image;
	}
    
	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	public String getPublicDescription() {
		return publicDescription;
	}

	public void setPublicDescription(String publicDescription) {
		this.publicDescription = publicDescription;
	}

	@PreRemove
	private void removeAuthorities() {
		authorities.clear();
		categories.clear();
	}
}
