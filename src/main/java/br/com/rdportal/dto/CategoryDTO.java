package br.com.rdportal.dto;

import br.com.rdportal.model.StorageFile;

public class CategoryDTO {
	
	Integer id;
	String name;
	String description;
	StorageFile image;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public StorageFile getImage() {
		return image;
	}
	public void setImage(StorageFile image) {
		this.image = image;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
