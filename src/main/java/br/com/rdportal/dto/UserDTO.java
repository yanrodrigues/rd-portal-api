package br.com.rdportal.dto;

import java.util.List;

import br.com.rdportal.model.Category;
import br.com.rdportal.model.StorageFile;
import br.com.rdportal.security.Authority;

public class UserDTO {
	
	private Integer id;
	private String username;
	private String password;
	private List<Authority> authorities;
	private String nomeCompleto = "";
	private String email = "";
	private StorageFile image;
	private List<Category> categories;
	private String publicName = "";
	private String publicDescription = "";
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
    }
    
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
    }
    
	public List<Authority> getAuthorities() {
		return authorities;
	}
	public void setAuthorities(List<Authority> authorities) {
		this.authorities = authorities;
    }
    
	public String getNomeCompleto() {
		return nomeCompleto;
	}
	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
    }
    
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
    }
    
	public StorageFile getImage() {
		return image;
	}
	public void setImage(StorageFile image) {
		this.image = image;
	}
	public List<Category> getCategories() {
		return categories;
	}
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	public String getPublicName() {
		return publicName;
	}
	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}
	public String getPublicDescription() {
		return publicDescription;
	}
	public void setPublicDescription(String publicDescription) {
		this.publicDescription = publicDescription;
	}
	
    
}