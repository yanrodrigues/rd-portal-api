package br.com.rdportal.dto;

import java.util.List;

import br.com.rdportal.enums.PostType;
import br.com.rdportal.model.Category;
import br.com.rdportal.model.PostForm;
import br.com.rdportal.model.PostText;

public class PostDTO {
	
	private Integer id;
	private PostType type;
	private PostText postText;
	private PostForm postForm;
	private List<Category> categories;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public PostText getPostText() {
		return postText;
	}

	public void setPostText(PostText postText) {
		this.postText = postText;
	}
	
	public PostForm getPostForm() {
		return postForm;
	}
	
	public void setPostForm(PostForm postForm) {
		this.postForm = postForm;
	}
	
	public PostType getType() {
		return type;
	}
	public void setType(PostType type) {
		this.type = type;
	}
	
	public List<Category> getCategories() {
		return categories;
	}
	
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	
}
