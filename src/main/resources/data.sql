INSERT INTO authority(id, name) VALUES(1, 'ROLE_USER');
INSERT INTO authority(id, name) VALUES(2, 'ROLE_MODERATOR');
INSERT INTO authority(id, name) VALUES(3, 'ROLE_ADMIN');

INSERT INTO usuario(id, email, nome_completo, password, username) VALUES (1, 'admin@rdportal.com', 'Administrador', '$2y$12$.HVsREp2754AeU7wWcktwO9alB50Kpu4cZjTrItdRAk1d0alNXcwW', 'admin');

INSERT INTO user_authority(user_id, authority_id) VALUES(1, 1);
INSERT INTO user_authority(user_id, authority_id) VALUES(1, 2);
INSERT INTO user_authority(user_id, authority_id) VALUES(1, 3);

ALTER SEQUENCE seq_usuario RESTART WITH 3;
ALTER SEQUENCE seq_authority RESTART WITH 4;